SALIR DE LA APLICACIÓN

SALIR
Ácido (L)
Base (L)
Temperatura (°C)
0.00
Agitador (RPM)
0
0
PARÁMETROS
0.00
0.00
0.00
Azúcar (L)
Levadura (L)
Ácido (L)
Base (L)
Temperatura (°C)
0.00
0
Parámetros
0.00
0.00
Solución
fermentada (L)
Solución C (ml)
Solución D (ml)
Solvente (ml)
0.00
Temperatura (°C)
0.00
Agitador (RPM)
0
0
PARÁMETROS
0.00
0.00
0.00
Solución A (ml)
Solución B (ml)
texto
texto
0
Usuario
00:00:00
Tiempo dispuesto 
Tiempo destilación
06:00:00
00:00:00
Condensador agua
0
0
7.00
Nivel espuma
Alcohol destilado (%)
pH

Indicadores
0
0.0
Alcohol etílico (L)

Oxígeno
0
Prueba de  síntesis (%)
0.0
Proceso aminoácido (ml)
INDICADORES
0.0
Acondicionamiento MP (ml)
INDICADORES
pH
7.00
0
Presión (PA)
Alcohol (%)
0
Tiempo dispuesto
04:00:00
Tiempo fermentación
00:00:00
Nivel Cultivo

Pantalla Digital
Termine el actual ejercicio

INICIAR
ESTACIÓN 1: TIRO PARABÓLICO
Reto
Terminar
fdasfdasdfaslkdfhalkshdflkashdflahsdlkfhaslkdfhalskhdflakshdflkahdfasfdhalskdhfasdhf




Evaluación
F
F
F
F
F
V
V
V
V
V
Reintentar
Continuar

Iniciar
Misión
Cómo jugar
asdfasdfasdfasdfasdfasdfasfdasfd
asdfasdfasdfasdfasdfasdfasfdasfdadsfasdfasdfasdfasdfadf
asdfasdfasdfasdfasdfasdfasfdasfd
asdfasdfasdfasdfasdfasdfasfdasfd
asdfasdfasdfasdfasdfasdfasfdasfd
asdfasdfasdfasdfasdfasdfasfdasfd
asdfasdfasdfasdfasdfasdfasfdasfd
asdfasdfasdfasdfasdfasdfasfdasfd
asdfasdfasdfasdfasdfasdfasfdasfd
fin




Calculadora
Ecuaciones
Gráfico
Instrucciones
0
1
2
3
4
5
6
7
8
9
C
Cos
/
=
+
-
*
.
Sin
√
0123456789.0 
0123456789.0 
0
0.12
100
Alcohol vs Tiempo
t
0.5
0.25
0.75
1
1.5
1.25
1.75
%
25
12.5
37.5
50
75
62.5
77.5
0
X
100
Alcohol vs Tiempo
t
0.5
0.25
0.75
1
1.5
1.25
1.75
%
25
12.5
37.5
50
75
62.5
77.5

alcohol
%  ———————     =
líquido a destilar
V alcohol
         ———————— * 100
V líquido a destilar
V alcohol: Volumen destilado de alcohol.
V líquido a destilar: Volumen solución fermentada a destilar.
% alcohol líquido a destilar: Porcentaje de alcohol a destilar.
V líquido a destilar
 ——————— 
V alcohol
a
     %  ————     =
sol
V a
         ———— * 100
V sol
% a/sol = Porcentaje de a en la solución
Va = Volumen de a.
Vsol = Volumen de solución.
Nota: La suma de los porcentajes de los componentes de una solución es 100.
C₆H₁₂O₆ ————> 2 CH₃ - CH₂OH + 2 CO₂
Medio(levaduras)
Glucosa
C₆H₁₂O₆
Gas carbónico
CO₂
Alcohol etílico o etanol
CH₃CH₂OH
0
0.12
100
Temperatura vs Tiempo
t
0.5
0.25
0.75
1
1.5
1.25
1.75
°C
25
12.5
37.5
50
75
62.5
82.5
Azúcar:
L
0
Levadura:
L
0
Temp:
C
0
RPM:
L
0
Base:
L
0
Ácido:
L
0
Solución:
L
0
Temp:
C
0
Base:
L
0
Ácido:
L
0
Solución A
Solución B
Solución C
Solución D
Temp
Agitador
Solvente
ml
0
ml
0
ml
0
C
0
ml
0
ml
0
rpm
0
Datos de simulación
Generar reporte
felipe andres 
00:00:00
1
Usuario:
Tiempo:
N° intentos:
LABORATORIO DE QUÍMICA
Usuario:
Institución:
Unidad:
Intentos:
0
Alcoholes, aldehídos y cetonas.
0

asdfassdfasdfasdfadfdfasdfasdfasdfasdf
0
Curso:
0
Fecha:
0
Id Curso:
Situación:
Preparación de una mezcla fermentada al 45% en alcohol
Tiempo:
0
Gráfica de lanzamiento
Tabla de datos


Preguntas de evaluación
Preguntas orientadoras
Resuelva las siguientes preguntas, anéxelas con la guía de aprendizaje a este reporte y envíelos a su profesor
 Enviar
Terminar
Visualizar
El medio de cultivo para la fermentación alcohólica de la práctica son microorganismos conocidos como levaduras.

La temperatura para que se produzca una fermentación alcohólica con levaduras es de 45 grados centígrados.

En una fermentación alcohólica se genera gas carbónico como subproducto, este se puede almacenar para industrializar.

Si encontramos un pH en la solución de fermentación de nueve (9) lo bajamos a su valor adecuado adicionando solución ácida.

El porcentaje de alcohol esperado para la solución fermentada es de  85%.
1. ¿Si la temperatura óptima para el medio de cultivo  en este práctica es de 35 grados las bacterias no hacen fermentación? Explique.

2. ¿La fermentación da el producto principal alcohol etílico, adicional gas carbónico, y también otros subproductos en cantidades muy pequeñas ?.  Explique su respuesta. 

3. ¿Si una fermentación no se controla adecuadamente se forma ácido etanoico? Explique.

4. ¿Qué tipo de fermentaciones se pueden llevar acabo en este tipo de fermentadores?

5. ¿Según la ecuación de fermentación si reaccionan 100gr de solución de azúcar cuanto alcohol y gas carbónico se producen?
Usuario

Curso

ID Curso

Institución

INICIAR
