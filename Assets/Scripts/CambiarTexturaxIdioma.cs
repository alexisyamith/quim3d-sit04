﻿using UnityEngine;
using System.Collections;

public class CambiarTexturaxIdioma : MonoBehaviour
{
    public ControlIdiomas controlIdioma;

    public Texture texturaIngles;
    public Texture texturaEspanol;
    public Texture texturaPortugues;

    void Awake()
    {
        applyTextures();
    }

    public void applyTextures()
    {
        if (controlIdioma.idioma == ControlIdiomas.Idioma.Espanol)
        {
            GetComponent<Renderer>().material.mainTexture = texturaEspanol;
        }
        if (controlIdioma.idioma == ControlIdiomas.Idioma.Ingles)
        {
            GetComponent<Renderer>().material.mainTexture = texturaIngles;
        }
        if (controlIdioma.idioma == ControlIdiomas.Idioma.Portugues)
        {
            GetComponent<Renderer>().material.mainTexture = texturaPortugues;
        }
    }
}
