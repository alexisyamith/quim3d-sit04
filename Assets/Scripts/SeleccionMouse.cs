﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace NSSeleccionMouse
{
	public class SeleccionMouse : MonoBehaviour 
	{
        #region members

        #endregion

        #region members serialized
        /// <summary>
        /// Layers que pueden ser seleccionadas
        /// </summary>
        [SerializeField, Tooltip("Layers que pueden ser seleccionadas")]
        private LayerMask layerMaskSeleccionable;
        /// <summary>
        /// Camara usada para el calculo del raycast que hace la seleccion
        /// </summary>
        [SerializeField, Tooltip("Camara usada para el calculo del raycast que hace la seleccion")]
        private Camera camaraSeleccion;
        #endregion

        #region delegates
        /// <summary>
        /// Selegado que se ejecuta cuando se selecciona algo
        /// </summary>
        private Action<GameObject> DltSeleccion = delegate(GameObject argGameObjectSeleccionado) { };
        #endregion

        #region properties

        public Action<GameObject> _DltSeleccion
        {
            set
            {
                DltSeleccion = value;
            }
            get
            {
                return DltSeleccion;
            }
        }

        #endregion

        #region methods Unity
	
		// Update is called once per frame
		void Update () 
		{
            Seleccionar();
		}

		#endregion

		#region methods public

		#endregion

		#region methods private

        private void Seleccionar()
        {
            if (Input.GetMouseButtonDown(0))
            {
                var tmpRaycastCamara = camaraSeleccion.ScreenPointToRay(Input.mousePosition);
                RaycastHit tmpRaycastHitOut;

                if (Physics.Raycast(tmpRaycastCamara, out tmpRaycastHitOut, float.MaxValue, layerMaskSeleccionable))
                {
                    DltSeleccion(tmpRaycastHitOut.collider.gameObject);
                }
            }
        }

		#endregion
	}
}
