﻿using UnityEngine;
using System.Collections;

public class CambiarTextoPh : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public SimuladorAminoAcido simuladorAminoAcido;
	public SimuladorDestilacion simuladorDestilacion;
	public SimuladorFermentacion simuladorFermentacion;


	string palabraCambiar = "PH";

	[ContextMenu("cambiar PH")]
	void CambiarTextoPH(){


		/*
		simuladorAminoAcido.textoComoJugarReto = simuladorAminoAcido.textoComoJugarReto.Replace(palabraCambiar , "pH");
		simuladorAminoAcido.textoComoJugarPracticaLibre = simuladorAminoAcido.textoComoJugarPracticaLibre.Replace(palabraCambiar , "pH");


		simuladorDestilacion.textoComoJugarReto = simuladorDestilacion.textoComoJugarReto.Replace(palabraCambiar , "pH");
		simuladorDestilacion.textoComoJugarPracticaLibre = simuladorDestilacion.textoComoJugarPracticaLibre.Replace(palabraCambiar , "pH");

		simuladorFermentacion.textoComoJugarReto = simuladorFermentacion.textoComoJugarReto.Replace(palabraCambiar , "pH");
		simuladorFermentacion.textoComoJugarPracticaLibre = simuladorFermentacion.textoComoJugarPracticaLibre.Replace(palabraCambiar , "pH");
		*/

		for ( int i =0 ; i < simuladorAminoAcido.listaPreguntas.Count ; i++   ){
			simuladorAminoAcido.listaPreguntas[i] = simuladorAminoAcido.listaPreguntas[i].Replace(palabraCambiar , "pH");
		}
		for ( int i =0 ; i < simuladorDestilacion.listaPreguntas.Count ; i++   ){
			simuladorDestilacion.listaPreguntas[i] = simuladorDestilacion.listaPreguntas[i].Replace(palabraCambiar , "pH");
		}

		for ( int i =0 ; i < simuladorFermentacion.listaPreguntas.Count ; i++   ){
			simuladorFermentacion.listaPreguntas[i] = simuladorFermentacion.listaPreguntas[i].Replace(palabraCambiar , "pH");
		}

		print("listo mijo");

	}

}
