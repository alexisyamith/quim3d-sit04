﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class Exportar : MonoBehaviour {


	[MenuItem("GenMedia/ExportarWeb")]
	static void ExportarWeb ()
	{
		string[] scenes = { "Assets/Escenas/SimuladorQuimica.unity" };
		//string rutaDrive = "/Users/innovativeeducation/Desktop/Exportacion/Web/Quimica/";
		//string rutaDrive = "C:/Users/lquinones/Desktop/Exportaciones/3DUnityExportations/Web/Quimica/";
		string rutaDrive = "/Users/innovativeeducation/Desktop/Export/_-CL2017/3D/";
		string nombre = "webPlayer";
		//BuildPipeline.BuildPlayer( scenes , rutaDrive + nombre , BuildTarget.WebPlayer , BuildOptions.WebPlayerOfflineDeployment );
	}

	[MenuItem("GenMedia/ExportarAndroid")]
	static void ExportarAndroid ()
	{

		string[] scenes = { "Assets/Escenas/SimuladorQuimica.unity" };
		//string rutaDrive = "/Users/innovativeeducation/Desktop/Exportacion/Android/Quimica/";
		//string rutaDrive = "C:/Users/lquinones/Desktop/Exportaciones/3DUnityExportations/Android/Quimica/";
		string rutaDrive = "/Users/innovativeeducation/Desktop/Export/_-CL2017/3D/";
		string nombre = "simuladorQuimica"+ UnityEditor.PlayerSettings.bundleVersion.ToString()  +".apk";
		BuildPipeline.BuildPlayer( scenes , rutaDrive + nombre  , BuildTarget.Android , BuildOptions.None   );

	}



}
